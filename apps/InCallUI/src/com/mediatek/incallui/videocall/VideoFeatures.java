package com.mediatek.incallui.videocall;

import com.android.incallui.Call;
import com.android.incallui.CallUtils;

/**
 * M: management for video call features.
 */
public class VideoFeatures {
    private final Call mCall;

    /**
     * M: [video call]for management of video call features.
     *
     * @param call the call associate with current VideoFeatures instance.
     */
    public VideoFeatures(Call call) {
        mCall = call;
    }

    /**
     * M: whether this call supports rotation.
     * make sure this is a video call before checking this feature.
     *
     * @return true if support.
     */
    public boolean supportsRotation() {
        return !isCsCall();
    }

    /**
     * M: whether this call supports downgrade.
     * make sure this is a video call before checking this feature.
     *
     * @return true if support.
     */
    public boolean supportsDowngrade() {
        return !isCsCall();
    }

    /**
     * M: whether this call supports answer as voice.
     * make sure this is a video call before checking this feature.
     *
     * @return true if support.
     */
    public boolean supportsAnswerAsVoice() {
        return !isCsCall();
    }

    /**
     * M: whether this call supports pause (turn off camera).
     * make sure this is a video call before checking this feature.
     *
     * @return
     */
    public boolean supportsPauseVideo() {
        return !isCsCall();
    }

    /**
     * M: whether this call supports upgrade.
     * make sure this is a voice call before checking this feature.
     *
     * @return
     */
    public boolean supportsUpgradeToVideo() {
        return !isCsCall();
    }

    /**
     * M: whether this call supports hold.
     * make sure this is a video call before checking this feature.
     *
     * @return
     */
    public boolean supportsHold() {
        return !isCsCall();
    }

    private boolean isCsCall() {
        return !mCall.hasProperty(android.telecom.Call.Details.PROPERTY_VOLTE);
    }

}
