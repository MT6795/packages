package com.android.server.telecom;

import com.android.server.telecom.components.ErrorDialogActivity;

import java.util.ArrayList;
import java.util.List;

import com.mediatek.telecom.volte.TelecomVolteUtils;
import com.mediatek.telecom.LogUtils;
import com.mediatek.telecom.TelecomManagerEx;
import com.mediatek.telecom.TelecomUtils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Trace;
import android.os.UserHandle;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telecom.VideoProfile;
import android.telephony.DisconnectCause;
import android.telephony.PhoneNumberUtils;
import android.widget.Toast;

/**
 * Single point of entry for all outgoing and incoming calls.
 * {@link com.android.server.telecom.components.UserCallIntentProcessor} serves as a trampoline that
 * captures call intents for individual users and forwards it to the {@link CallIntentProcessor}
 * which interacts with the rest of Telecom, both of which run only as the primary user.
 */
public class CallIntentProcessor {

    private static final String TAG = "CallIntentProcessor";
    public static final String KEY_IS_UNKNOWN_CALL = "is_unknown_call";
    public static final String KEY_IS_INCOMING_CALL = "is_incoming_call";
    /*
     *  Whether or not the dialer initiating this outgoing call is the default dialer, or system
     *  dialer and thus allowed to make emergency calls.
     */
    public static final String KEY_IS_PRIVILEGED_DIALER = "is_privileged_dialer";

    private final Context mContext;
    private final CallsManager mCallsManager;

    public CallIntentProcessor(Context context, CallsManager callsManager) {
        this.mContext = context;
        this.mCallsManager = callsManager;
    }

    public void processIntent(Intent intent) {
        final boolean isUnknownCall = intent.getBooleanExtra(KEY_IS_UNKNOWN_CALL, false);
        Log.i(this, "onReceive - isUnknownCall: %s", isUnknownCall);

        Trace.beginSection("processNewCallCallIntent");
        if (isUnknownCall) {
            processUnknownCallIntent(mCallsManager, intent);
        } else {
            processOutgoingCallIntent(mContext, mCallsManager, intent);
        }
        Trace.endSection();
    }


    /**
     * Processes CALL, CALL_PRIVILEGED, and CALL_EMERGENCY intents.
     *
     * @param intent Call intent containing data about the handle to call.
     */
    static void processOutgoingCallIntent(
            Context context,
            CallsManager callsManager,
            Intent intent) {

        /// M: for log parser @{
        LogUtils.logIntent(intent);
        /// @}

        if (shouldPreventDuplicateVideoCall(context, callsManager, intent)) {
            return;
        }

        Uri handle = intent.getData();
        String scheme = handle.getScheme();
        String uriString = handle.getSchemeSpecificPart();

        if (!PhoneAccount.SCHEME_VOICEMAIL.equals(scheme)) {
            handle = Uri.fromParts(PhoneNumberUtils.isUriNumber(uriString) ?
                    PhoneAccount.SCHEME_SIP : PhoneAccount.SCHEME_TEL, uriString, null);
        }

        PhoneAccountHandle phoneAccountHandle = intent.getParcelableExtra(
                TelecomManager.EXTRA_PHONE_ACCOUNT_HANDLE);

        /// M: for volte: IMS only @{
        if (TelecomVolteUtils.isImsCallOnlyRequest(intent)
                || TelecomVolteUtils.isConferenceDialRequest(intent)) {
            Log.d(TAG, "MO - VoLTE case: Ims only or Conference dial request.");

            // If Ims is disabled in setting, notify user to enable it.
            if (!TelecomVolteUtils.isImsEnabled(context)) {
                Log.d(TAG, "MO - VoLTE case: Ims is disabled => Abandon");
                TelecomVolteUtils.showImsDisableDialog(context);
                return;
            }
            // Ims is enabled, try to get a PhoneAccount to dial it.
            List<PhoneAccountHandle> voltePhoneAccountHandles = TelecomUtils
                    .getVoltePhoneAccountHandles();

            // For now, we only have one PhoneAccount to support VoLTE at most,
            // If we have more than one phoneAccount support VoLTE later,
            // then we should review phoneAccount-select part for ims call only.
            Log.d(TAG, "MO - VoLTE case: Size of VoLTE PhoneAccountHandles: "
                    + voltePhoneAccountHandles.size());
            switch (voltePhoneAccountHandles.size()) {
                case 0:
                    // no VoLTE phoneAccount, so abandon this dial request.
                    TelecomVolteUtils.showNoImsServiceDialog(context);
                    return;
                case 1:
                    // one VoLTE phoneAccount, use it.
                    Log.d(TAG, "MO - VoLTE case: phoneAccountHandleChanged: "
                            + phoneAccountHandle + " => " + voltePhoneAccountHandles.get(0));
                    phoneAccountHandle = voltePhoneAccountHandles.get(0);
                    handle = Uri.fromParts(PhoneAccount.SCHEME_TEL, uriString, null);
                    break;
                default:
                    // For now, should not come here, need check!.
                    break;
            }
        }

        // For VoLTE conference dial, we have already get the only PhoneAccount above. dial via it.
        if (TelecomVolteUtils.isConferenceDialRequest(intent)) {
            List<String> numbers = TelecomVolteUtils.getConferenceDialNumbers(intent);
            if (numbers != null && !numbers.isEmpty()) {
                Call call = TelecomSystem.getInstance().getCallsManager().
                          placeOutgoingConferenceCall(phoneAccountHandle, numbers);
            } else {
                // TODO: maybe we should goto ErrorDialogActivity
                Log.d(TAG, "MO - VoLTE case: no number detected => Abandon!");
            }
            // For conference dial, we skip NewOutgoingCallIntentBroadcaster.
            return;
        }
        /// @}

        /// M: For dial via specified slot. @{
        if (intent.hasExtra(TelecomUtils.EXTRA_SLOT)) {
            int slotId = intent.getIntExtra(TelecomUtils.EXTRA_SLOT, -1);
            phoneAccountHandle = TelecomUtils
                    .getPhoneAccountHandleWithSlotId(context, slotId, phoneAccountHandle);
        }
        /// @}

        Bundle clientExtras = null;
        if (intent.hasExtra(TelecomManager.EXTRA_OUTGOING_CALL_EXTRAS)) {
            clientExtras = intent.getBundleExtra(TelecomManager.EXTRA_OUTGOING_CALL_EXTRAS);
        }
        if (clientExtras == null) {
            clientExtras = new Bundle();
        }

        final boolean isPrivilegedDialer = intent.getBooleanExtra(KEY_IS_PRIVILEGED_DIALER, false);

        /// M: For Ip dial @{
        clientExtras.putBoolean(Constants.EXTRA_IS_IP_DIAL,
                   intent.getBooleanExtra(Constants.EXTRA_IS_IP_DIAL, false));
        /// @}

        /// M: Supporting suggested account @{
        PhoneAccountHandle suggestedPhoneAccountHandle = intent.getParcelableExtra(
                TelecomManagerEx.EXTRA_SUGGESTED_PHONE_ACCOUNT_HANDLE);
        if (suggestedPhoneAccountHandle != null) {
            clientExtras.putParcelable(TelecomManagerEx.EXTRA_SUGGESTED_PHONE_ACCOUNT_HANDLE,
                                    suggestedPhoneAccountHandle);
        }
        /// @}

        // Send to CallsManager to ensure the InCallUI gets kicked off before the broadcast returns
        Call call = callsManager.startOutgoingCall(handle, phoneAccountHandle, clientExtras);

        if (call != null) {
            /// M: ip dial. ip prefix already add, here need to change intent @{
            if (call.isIpCall()) {
                intent.setData(call.getHandle());
            }
            /// @}

            // Asynchronous calls should not usually be made inside a BroadcastReceiver
            // because once
            // onReceive is complete, the BroadcastReceiver's process runs the risk of getting
            // killed if memory is scarce. However, this is OK here because the entire Telecom
            // process will be running throughout the duration of the phone call and should never
            // be killed.
            NewOutgoingCallIntentBroadcaster broadcaster = new NewOutgoingCallIntentBroadcaster(
                    context, callsManager, call, intent, isPrivilegedDialer);

            final int result = broadcaster.processIntent();
            final boolean success = result == DisconnectCause.NOT_DISCONNECTED;

            if (!success && call != null) {
                disconnectCallAndShowErrorDialog(context, call, result);
            }
        }
    }

    static void processIncomingCallIntent(CallsManager callsManager, Intent intent) {

        /// M: for log parser @{
        LogUtils.logIntent(intent);
        /// @}

        PhoneAccountHandle phoneAccountHandle = intent.getParcelableExtra(
                TelecomManager.EXTRA_PHONE_ACCOUNT_HANDLE);

        if (phoneAccountHandle == null) {
            Log.w(CallIntentProcessor.class,
                    "Rejecting incoming call due to null phone account");
            return;
        }
        if (phoneAccountHandle.getComponentName() == null) {
            Log.w(CallIntentProcessor.class,
                    "Rejecting incoming call due to null component name");
            return;
        }

        Bundle clientExtras = null;
        if (intent.hasExtra(TelecomManager.EXTRA_INCOMING_CALL_EXTRAS)) {
            clientExtras = intent.getBundleExtra(TelecomManager.EXTRA_INCOMING_CALL_EXTRAS);
        }
        if (clientExtras == null) {
            clientExtras = new Bundle();
        }

        Log.d(CallIntentProcessor.class,
                "Processing incoming call from connection service [%s]",
                phoneAccountHandle.getComponentName());
        callsManager.processIncomingCallIntent(phoneAccountHandle, clientExtras);
    }

    static void processUnknownCallIntent(CallsManager callsManager, Intent intent) {
        PhoneAccountHandle phoneAccountHandle = intent.getParcelableExtra(
                TelecomManager.EXTRA_PHONE_ACCOUNT_HANDLE);

        if (phoneAccountHandle == null) {
            Log.w(CallIntentProcessor.class, "Rejecting unknown call due to null phone account");
            return;
        }
        if (phoneAccountHandle.getComponentName() == null) {
            Log.w(CallIntentProcessor.class, "Rejecting unknown call due to null component name");
            return;
        }

        callsManager.addNewUnknownCall(phoneAccountHandle, intent.getExtras());
    }

    private static void disconnectCallAndShowErrorDialog(
            Context context, Call call, int errorCode) {
        call.disconnect();
        final Intent errorIntent = new Intent(context, ErrorDialogActivity.class);
        int errorMessageId = -1;
        switch (errorCode) {
            case DisconnectCause.INVALID_NUMBER:
            case DisconnectCause.NO_PHONE_NUMBER_SUPPLIED:
                errorMessageId = R.string.outgoing_call_error_no_phone_number_supplied;
                break;
        }
        if (errorMessageId != -1) {
            errorIntent.putExtra(ErrorDialogActivity.ERROR_MESSAGE_ID_EXTRA, errorMessageId);
            errorIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivityAsUser(errorIntent, UserHandle.CURRENT);
        }
    }

    /**
     * Whether an outgoing video call should be prevented from going out. Namely, don't allow an
     * outgoing video call if there is already an ongoing video call. Notify the user if their call
     * is not sent.
     *
     * @return {@code true} if the outgoing call is a video call and should be prevented from going
     *     out, {@code false} otherwise.
     */
    private static boolean shouldPreventDuplicateVideoCall(
            Context context,
            CallsManager callsManager,
            Intent intent) {

        /// M: For ViLTE @{
        // always allow VT-MO while VT exist
        // Original Code:
        /*
        int intentVideoState = intent.getIntExtra(TelecomManager.EXTRA_START_CALL_WITH_VIDEO_STATE,
                VideoProfile.STATE_AUDIO_ONLY);
        if (VideoProfile.isAudioOnly(intentVideoState)
                || !callsManager.hasVideoCall()) {
            return false;
        } else {
            // Display an error toast to the user.
            Toast.makeText(
                    context,
                    context.getResources().getString(R.string.duplicate_video_call_not_allowed),
                    Toast.LENGTH_LONG).show();
            return true;
        } */

        return false;
        /// @}
    }
}
